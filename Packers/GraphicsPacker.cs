﻿using Bitz.Modules.Core.Foundation.Pack;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Bitz.Tools.Packer.Packers
{
    class GraphicsPacker
    {
        public const Int32 PACK_SIZE = 4096;

        public GraphicsPack Pack(String[] fileNames, String graphicsPackName)
        {
            List<TexturePack> returnTexturePacks = new List<TexturePack>();

            List<TexturePack.PackedElement> texturesToPack = new List<TexturePack.PackedElement>();
            foreach (String s in fileNames)
            {
                using (Bitmap sourceBmp = new Bitmap(s))
                {
                    TexturePack.PackedElement element = new TexturePack.PackedElement
                    {
                        OrigionalURL = s,
                        Width = sourceBmp.Width + 2,
                        Height = sourceBmp.Height + 2,
                        X = 0,
                        Y = 0
                    };
                    texturesToPack.Add(element);
                }
            }

            texturesToPack = texturesToPack.OrderByDescending(a => a.Width * a.Height).ToList();

            while (texturesToPack.Count > 0)
            {
                Boolean packed = false;
                TexturePack.PackedElement pe = texturesToPack[0];

                if (Path.GetFileName(pe.OrigionalURL).StartsWith("_"))
                {
                    TexturePack soloPack = new TexturePack
                    {
                        Compression = TexturePack.CompressionFormat.RAW,
                        Width = pe.Width-2,
                        Height = pe.Height-2
                    };
                    pe.X = 0;
                    pe.Y = 0;
                    soloPack.AddElement(pe);
                    returnTexturePacks.Add(soloPack);
                    packed = true;
                }
                else
                {

                    foreach (TexturePack pack in returnTexturePacks)
                    {
                        List<TexturePack.PackedElement> existing = pack.GetPackedElements().ToList();

                        for (Int32 x = 0; x < pack.Width - pe.Width; x += 8)
                        {
                            for (Int32 y = 0; y < pack.Height - pe.Height; y += 8)
                            {
                                Rectangle packingTarget = new Rectangle(x, y, pe.Width, pe.Height);
                              
                                Boolean canpack = existing.All( e=>Rectangle.Intersect(packingTarget,new Rectangle(e.X,e.Y,e.Width,e.Height))==Rectangle.Empty);
                                if (canpack)
                                {
                                    pe.X = x;
                                    pe.Y = y;
                                    pack.AddElement(pe);
                                    packed = true;
                                }
                                if (packed) break;
                            }
                            if (packed) break;
                        }
                    }
                }
                if (packed)
                {
                    texturesToPack.RemoveAt(0);
                }
                else
                {
                    returnTexturePacks.Add(new TexturePack
                    {
                        Compression = TexturePack.CompressionFormat.RAW,
                        Width = PACK_SIZE,
                        Height = PACK_SIZE
                    });
                }
            }

            return new GraphicsPack
            {
                Name = graphicsPackName,
                TexturePacks = returnTexturePacks
            };
        }
    }
}
