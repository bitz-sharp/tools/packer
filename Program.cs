﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Bitz.Modules.Core.Foundation.Pack;
using Bitz.Tools.Packer.Compressor;
using Bitz.Tools.Packer.Packers;
using Newtonsoft.Json;
using Sbatman.Serialize;

namespace Bitz.Tools.Packer
{
    class Program
    {
        public const String FOLDERS_GRAPHICS = "Graphics";
        public const String FOLDERS_OUTPUT = "Packed";
        public const String FOLDERS_WEB = "Web";
        public const String FOLDERS_PREVIEW = FOLDERS_OUTPUT + "/Preview";

        private static void Main()
        {
            if (IntPtr.Size != 8)
            {
                Console.WriteLine("It appears the packer has been built for X86 rather than X64 and will now exit. Please ensure it has been built with the Windows-X64 configuration");
                Console.ReadKey();
                return;
            }

            List<GraphicsPack> graphicsPacks = new List<GraphicsPack>();
            FindAndPackTextures(graphicsPacks);
            ProduceAtlasTextures(graphicsPacks);

            List<Compressor.Compressor> activeCompressors = new List<Compressor.Compressor> { new RawCompressor(), new DXTCompressor() };

            CompressAndExportGraphicsPacks(graphicsPacks, activeCompressors);
        }

        private static void CompressAndExportGraphicsPacks(IEnumerable<GraphicsPack> graphicsPacks, List<Compressor.Compressor> compressors)
        {
            if (!Directory.Exists(FOLDERS_OUTPUT)) Directory.CreateDirectory(FOLDERS_OUTPUT);
            if (!Directory.Exists(FOLDERS_WEB)) Directory.CreateDirectory(FOLDERS_WEB);
            if (!Directory.Exists(FOLDERS_PREVIEW)) Directory.CreateDirectory(FOLDERS_PREVIEW);

            foreach (GraphicsPack graphicsPack in graphicsPacks)
            {

                foreach (Compressor.Compressor compressor in compressors)
                {
                    Int32 tpID = 0;
                    if (!Directory.Exists(FOLDERS_OUTPUT + "/" + compressor.CompressionName())) Directory.CreateDirectory(FOLDERS_OUTPUT + "/" + compressor.CompressionName());

                    GraphicsPack compressedGraphicsPack = new GraphicsPack { Name = graphicsPack.Name };
                    foreach (TexturePack texturePack in graphicsPack.TexturePacks)
                    {
                        TexturePack compressedTexturePack = new TexturePack();
                        foreach (TexturePack.PackedElement element in texturePack.GetPackedElements())
                        {
                            compressedTexturePack.AddElement(element);
                        }

                        compressedTexturePack.SetSourceTextureMemory(compressor.Compress(texturePack.GetSourceTextureMemory(), texturePack.Width, texturePack.Height));
                        compressedTexturePack.Compression = compressor.GetCompressionFormat();
                        compressedTexturePack.Width = texturePack.Width;
                        compressedTexturePack.Height = texturePack.Height;

                        if (compressor is RawCompressor)
                        {
                            Bitmap returnBitMap = new Bitmap(texturePack.Width, texturePack.Height, PixelFormat.Format32bppArgb);
                            BitmapData bmData = returnBitMap.LockBits(new Rectangle(0, 0, returnBitMap.Width, returnBitMap.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                            IntPtr pNative = bmData.Scan0;
                            byte[] imgData = new Byte[texturePack.Width * texturePack.Height * 4];
                            Array.Copy(compressedTexturePack.GetSourceTextureMemory(), imgData, texturePack.Width * texturePack.Height * 4);
                            for (int i = 0; i < texturePack.Width * texturePack.Height; i++)
                            {
                                int r = imgData[(i * 4) ];
                                int b = imgData[(i * 4) + 2];
                                imgData[(i * 4) + 0] = (byte)b;
                                imgData[(i * 4) + 2] = (byte)r;
                            }
                            Marshal.Copy(imgData, 0, pNative, texturePack.Width * texturePack.Height * 4);
                            returnBitMap.UnlockBits(bmData);

                            returnBitMap.Save($"{FOLDERS_PREVIEW}/{graphicsPack.Name}{tpID++}.bmp");
                            returnBitMap.Save($"{FOLDERS_WEB}/{graphicsPack.Name}{tpID++}.png");
                        }

                        compressedGraphicsPack.TexturePacks.Add(compressedTexturePack);
                    }

                    Packet packet = compressedGraphicsPack.ToPacket();
                    Byte[] saveData = packet.ToByteArray();
                    packet.Dispose();

                    String filenameJson = $"{FOLDERS_WEB}/{graphicsPack.Name}.json";
                    String filenameDat = $"{FOLDERS_OUTPUT}/{compressor.CompressionName()}/{graphicsPack.Name}.pack";
                    if (File.Exists(filenameJson)) File.Delete(filenameJson);
                    if (File.Exists(filenameDat)) File.Delete(filenameDat);

                    GFXPK jsonGraphicsPack = new GFXPK()
                    {
                        TexturePacks = compressedGraphicsPack.TexturePacks.Select(a =>
                        {
                            return new GFXPK.TEXPK()
                            {
                                Textures = a.GetPackedElements().Select(b => new GFXPK.TEXPK.TEX()
                                {
                                    X = b.X,
                                    Y = b.Y,
                                    Width = b.Width,
                                    Height = b.Height,
                                    Name = b.OrigionalURL
                                }).ToList()
                            };
                        }).ToList()
                    };

                    File.WriteAllText(filenameJson, JsonConvert.SerializeObject(jsonGraphicsPack));

                    using (FileStream outStream = new FileStream(filenameDat, FileMode.CreateNew))
                    {
                        outStream.Write(saveData, 0, saveData.Length);
                    }

                    compressedGraphicsPack.Dispose();

                }
                graphicsPack.Dispose();
            }
        }

        struct GFXPK
        {
            public struct TEXPK
            {
                public struct TEX
                {
                    public Single X;
                    public Single Y;
                    public Single Width;
                    public Single Height;
                    public String Name;
                }

                public List<TEX> Textures;
            }

            public List<TEXPK> TexturePacks;
        }

        private static void ProduceAtlasTextures(IEnumerable<GraphicsPack> graphicsPacks)
        {
            foreach (TexturePack texturePack in graphicsPacks.SelectMany(graphicsPack => graphicsPack.TexturePacks))
            {
                using (Bitmap atlasTexture = new Bitmap(texturePack.Width, texturePack.Height, PixelFormat.Format32bppArgb))
                {
                    Graphics g = Graphics.FromImage(atlasTexture);
                    g.CompositingMode = CompositingMode.SourceCopy;

                    foreach (TexturePack.PackedElement element in texturePack.GetPackedElements())
                    {
                        using (Bitmap src = new Bitmap(element.OrigionalURL))
                        {
                            if (texturePack.Width != element.Width || texturePack.Height != element.Height)
                            {
                                g.DrawImage(src, new Rectangle(element.X, element.Y + 1, 1, element.Height - 2), 0, 0, 1, element.Height - 2, GraphicsUnit.Pixel);
                                g.DrawImage(src, new Rectangle(element.X + 1, element.Y, element.Width - 2, 1), 0, 0, element.Width - 2, 1, GraphicsUnit.Pixel);
                                g.DrawImage(src, new Rectangle(element.X + element.Width - 1, element.Y + 1, 1, element.Height - 2), element.Width - 3, 0, 1, element.Height - 2, GraphicsUnit.Pixel);
                                g.DrawImage(src, new Rectangle(element.X + 1, element.Y + element.Height - 1, element.Width - 2, 1), 0, element.Height - 3, element.Width - 2, 1, GraphicsUnit.Pixel);
                                g.DrawImage(src, element.X + 1, element.Y + 1, element.Width - 2, element.Height - 2);

                                // remove the whitespace padding before packing
                                element.X += 1;
                                element.Y += 1;
                                element.Width -= 2;
                                element.Height -= 2;
                            }
                            else
                            {
                                g.DrawImage(src, 0, 0, element.Width, element.Height);
                            }




                        }

                        BitmapData bmData = atlasTexture.LockBits(new Rectangle(0, 0, atlasTexture.Width, atlasTexture.Height), ImageLockMode.ReadWrite, atlasTexture.PixelFormat);
                        IntPtr pNative = bmData.Scan0;
                        Byte[] data = new Byte[atlasTexture.Width * atlasTexture.Height * 4];
                        Marshal.Copy(pNative, data, 0, atlasTexture.Width * atlasTexture.Height * 4);
                        atlasTexture.UnlockBits(bmData);

                        texturePack.SetSourceTextureMemory(data);
                    }
                    g.Dispose();

                }
            }
        }

        private static void FindAndPackTextures(List<GraphicsPack> graphicsPacks)
        {
            List<Task> packingTask = new List<Task>();

            foreach (String directory in Directory.GetDirectories($"{FOLDERS_GRAPHICS}"))
            {
                List<String> graphicsFiles = new List<String>();
                GetFilesRecursivly(directory, graphicsFiles);
                Task t = new Task(() =>
                {
                    GraphicsPacker graphicsPacker = new GraphicsPacker();
                    GraphicsPack gp = graphicsPacker.Pack(graphicsFiles.ToArray().Where(a => a.EndsWith(".png")).ToArray(), Path.GetFileName(directory) ?? "");
                    lock (graphicsPacks)
                    {
                        graphicsPacks.Add(gp);
                    }
                });
                lock (packingTask) packingTask.Add(t);
                t.Start();
            }
            Boolean stillPacking = true;
            while (stillPacking)
            {
                lock (packingTask) stillPacking = packingTask.Any(a => !a.IsCompleted);
                Thread.Sleep(1);
            }
        }

        private static void GetFilesRecursivly(String directoryToSearch, List<String> filesFound)
        {
            filesFound.AddRange(Directory.GetFiles(directoryToSearch));
            foreach (String directory in Directory.GetDirectories(directoryToSearch)) GetFilesRecursivly(directory, filesFound);
        }
    }
}
