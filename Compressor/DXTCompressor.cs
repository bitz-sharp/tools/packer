﻿using Bitz.Modules.Core.Foundation.Pack;
using System;
using System.Runtime.InteropServices;

namespace Bitz.Tools.Packer.Compressor
{
    public class DXTCompressor : Compressor
    {
        internal static class NativeCode
        {
            [DllImport("squish.dll", EntryPoint = "CompressImage")]
            internal static extern void CompressImage(IntPtr inDat, Int32 width, Int32 height, IntPtr outDat, Int32 flags);
        }

        public override Byte[] Compress(Byte[] sourceArray, Int32 width, Int32 height)
        {
            Byte[] inputArray = new Byte[sourceArray.Length];

            for (Int32 i = 0; i < sourceArray.Length; i += 4)
            {
                inputArray[i] = sourceArray[i+2];
                inputArray[i + 1] = sourceArray[i + 1];
                inputArray[i + 2] = sourceArray[i ];
                inputArray[i + 3] = sourceArray[i + 3];
            }

            Int32 resultSize = width * height;
            Byte[] returnData = new Byte[resultSize];

            IntPtr srcPTR = Marshal.AllocHGlobal(inputArray.Length);
            IntPtr resPTR = Marshal.AllocHGlobal(resultSize);
            Marshal.Copy(inputArray, 0, srcPTR, inputArray.Length);

            NativeCode.CompressImage(srcPTR, width, height, resPTR, 4 | 8);
            Marshal.Copy(resPTR, returnData, 0, resultSize);
            Marshal.FreeHGlobal(srcPTR);
            Marshal.FreeHGlobal(resPTR);

            return returnData;
        }

        public override String CompressionName()
        {
            return "DXT";
        }

        public override TexturePack.CompressionFormat GetCompressionFormat()
        {
            return TexturePack.CompressionFormat.DXT;
        }
    }
}
