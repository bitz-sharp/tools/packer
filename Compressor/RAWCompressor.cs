﻿using Bitz.Modules.Core.Foundation.Pack;
using System;

namespace Bitz.Tools.Packer.Compressor
{
    class RawCompressor : Compressor
    {
        public override Byte[] Compress(Byte[] sourceArray, Int32 width, Int32 height)
        {
            Byte[] outArray = new Byte[sourceArray.Length];

            for (Int32 i = 0; i < sourceArray.Length; i += 4)
            {
                outArray[i] = sourceArray[i + 2];
                outArray[i+1] = sourceArray[i + 1];
                outArray[i+2] = sourceArray[i ];
                outArray[i+3] = sourceArray[i + 3];
            }

            return outArray;
        }

        public override String CompressionName()
        {
            return "BMP";
        }

        public override TexturePack.CompressionFormat GetCompressionFormat()
        {
            return TexturePack.CompressionFormat.RAW;
        }
    }
}
