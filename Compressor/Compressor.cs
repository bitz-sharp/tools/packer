﻿using System;
using Bitz.Modules.Core.Foundation.Pack;

namespace Bitz.Tools.Packer.Compressor
{
    public abstract class Compressor
    {
        public abstract Byte[] Compress(Byte[] sourceArray, Int32 width, Int32 height);
        public abstract String CompressionName();
        public abstract TexturePack.CompressionFormat GetCompressionFormat();
    }
}
